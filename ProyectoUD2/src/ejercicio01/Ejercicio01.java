package ejercicio01;
public class Ejercicio01 {
	public static void main(String[] args) {
		// TODO Auto-generated method stub
		Ejercicio01.contarPalabras();
		System.out.println(" ");
		Ejercicio01.espaciarPalabras();
		System.out.println(" ");
		Ejercicio01.InvertirPalabra();
	}
public static void contarPalabras() {
	String cadena="hola joel cadena caracola adios";
	String[] separador=cadena.split(" ");
	int total=0;
	for(int i=0;i<separador.length;i++) {
		total++;
	}
	System.out.println("El total de palabras del String es "+total);
}
public static void espaciarPalabras() {
	String palabra="caracola";
	String espacios=palabra.replace("", " ");
	System.out.println("La palabra sin espacios "+ palabra);
	System.out.println("La palabra con espacios "+espacios);
}
public static void InvertirPalabra() {
	String cadena = "perro gato";
	int separador=cadena.indexOf(' ');
	String palabra1=cadena.substring(0,separador);
	String palabra2=cadena.substring(separador+1);
	System.out.println(cadena);
	System.out.println(palabra2+" "+palabra1);
}
}
